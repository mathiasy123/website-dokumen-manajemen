-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 23 Jun 2018 pada 07.47
-- Versi Server: 5.6.21
-- PHP Version: 5.5.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `app_dokumen`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `divisi`
--

CREATE TABLE IF NOT EXISTS `divisi` (
`id` int(11) NOT NULL,
  `nama` varchar(500) NOT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `divisi`
--

INSERT INTO `divisi` (`id`, `nama`, `user_id`) VALUES
(1, 'Admin', 1),
(2, 'Pemasaran', 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `docs`
--

CREATE TABLE IF NOT EXISTS `docs` (
`id` int(11) NOT NULL,
  `judul` varchar(50) NOT NULL,
  `deskripsi` varchar(500) NOT NULL,
  `file` varchar(255) NOT NULL,
  `tanggal` date NOT NULL,
  `user_id` int(11) NOT NULL,
  `kategori_id` int(11) DEFAULT NULL,
  `divisi_id` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `docs`
--

INSERT INTO `docs` (`id`, `judul`, `deskripsi`, `file`, `tanggal`, `user_id`, `kategori_id`, `divisi_id`) VALUES
(1, 'Penawaran 2018', 'Penawaran Produk Stellar 2018', 'file_docs/Penawaran 2018.rtf', '2018-06-22', 1, 4, 1),
(2, 'Pemasaran 2018', 'Pemasaran promosi Produk Stellar', 'file_docs/Pemasaran 2018.txt', '2018-06-23', 1, 1, NULL),
(3, 'Event 2018', 'Event untuk pemasaran produk Stellar 2018', 'file_docs/Event 2018.txt', '2018-06-22', 1, 6, 1),
(4, 'Penawaran 2018', 'Penawaran Produk Stellar 2018', 'file_docs/Penawaran 2018.rtf', '2018-06-23', 1, 4, 1),
(5, 'Rapat Pemasaran', 'Rapat untuk membuat strategi pemasaran produk Stellar 2018', 'file_docs/Rapat Pemasaran.docx', '2018-06-22', 1, 2, 1),
(6, 'Pemasaran 2019', 'Strategi pemasaran untuk 2019', 'file_docs/Pemasaran 2019.docx', '2018-06-22', 1, 1, NULL),
(7, 'Keuangan Pribadi 2017-2018', 'Keuangan pengeluaran dan penghasilan', 'file_docs/Keuangan Pribadi 2017-2018.docx', '2018-06-22', 1, 5, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `kategori`
--

CREATE TABLE IF NOT EXISTS `kategori` (
`id` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `divisi_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kategori`
--

INSERT INTO `kategori` (`id`, `nama`, `divisi_id`, `user_id`) VALUES
(1, 'Pemasaran', NULL, 1),
(2, 'Pemasaran', 1, 1),
(3, 'Penawaran', NULL, 1),
(4, 'Penawaran', 1, 1),
(5, 'Keuangan', NULL, 1),
(6, 'Event', 1, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE IF NOT EXISTS `user` (
`id` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(100) NOT NULL,
  `foto` varchar(255) NOT NULL,
  `member` date NOT NULL,
  `divisi_id` int(11) DEFAULT NULL,
  `log` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`id`, `nama`, `email`, `password`, `foto`, `member`, `divisi_id`, `log`) VALUES
(1, 'Admin', 'admin123@gmail.com', '$2y$10$v.uSri4O/t.MfVKhVAE9deA0JoO4dG/uoelEAoXQyZSTdRwaYIQ92', 'image/uploads/Admin.jpg', '2018-06-20', 1, 1529732859),
(2, 'Leader Division', 'leader123@gmail.com', '$2y$10$F35atnyVfSTikU0cYOpTm.Gw68niEya.C94AioBE3ZmFD/wRD453i', 'image/uploads/Leader Division.jpg', '2018-06-22', 2, NULL),
(4, 'Member', 'member@gmail.com', '$2y$10$Lb.BRfEFHXkEVdgx9/EURePXNuTDpbMZhi19OnwHwU8uzOR9p2rK6', 'image/uploads/Member.jpg', '2018-06-22', 2, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `divisi`
--
ALTER TABLE `divisi`
 ADD PRIMARY KEY (`id`), ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `docs`
--
ALTER TABLE `docs`
 ADD PRIMARY KEY (`id`), ADD KEY `user_id` (`user_id`), ADD KEY `kategori_id` (`kategori_id`), ADD KEY `divisi_id` (`divisi_id`);

--
-- Indexes for table `kategori`
--
ALTER TABLE `kategori`
 ADD PRIMARY KEY (`id`), ADD KEY `divisi_id` (`divisi_id`), ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
 ADD PRIMARY KEY (`id`), ADD KEY `divisi_id` (`divisi_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `divisi`
--
ALTER TABLE `divisi`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `docs`
--
ALTER TABLE `docs`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `kategori`
--
ALTER TABLE `kategori`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `divisi`
--
ALTER TABLE `divisi`
ADD CONSTRAINT `divisi_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE SET NULL ON UPDATE SET NULL;

--
-- Ketidakleluasaan untuk tabel `docs`
--
ALTER TABLE `docs`
ADD CONSTRAINT `docs_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON UPDATE CASCADE,
ADD CONSTRAINT `docs_ibfk_2` FOREIGN KEY (`kategori_id`) REFERENCES `kategori` (`id`) ON UPDATE CASCADE,
ADD CONSTRAINT `docs_ibfk_3` FOREIGN KEY (`divisi_id`) REFERENCES `divisi` (`id`) ON DELETE SET NULL ON UPDATE SET NULL;

--
-- Ketidakleluasaan untuk tabel `kategori`
--
ALTER TABLE `kategori`
ADD CONSTRAINT `kategori_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON UPDATE CASCADE,
ADD CONSTRAINT `kategori_ibfk_2` FOREIGN KEY (`divisi_id`) REFERENCES `divisi` (`id`) ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
