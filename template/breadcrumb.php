<!-- breadcrumb.php -->
<?php 
	$user = "";
	$doc = "";
	$division = "";
	$kategori = "";
	$home = "";
	$tambah = "";
	$edit = "";
	$profil = "";

	if(isset($_GET['page'])) {
    $page = $_GET['page'];
  } else {
    $page = "";
  }

  switch($page) {
    case "docs":
    	$doc = "Document"; break;
    case "kat":
    	$kategori = "Category"; break;
    case "div":
    	$division = "Division"; break;
    case "user":
    	$user = "User"; break;
    	
    case "add_docs":
    case "docs":
    	$doc = "Document";
    	$tambah = "Add"; break;

    case "add_kat":
		case "kat":
		$kategori = "Category";
	    $tambah = "Add"; break;

    case "add_div":
    case "div":
	    $division = "Divisi";
	    $tambah = "Add"; break;

    case "add_user":
    case "user":
    	$user = "User";
        $tambah = "Tambah"; break;

    case "edit_docs":
    case "docs":
    	$doc = "Document";
    	$edit = "Update"; break;

    case "edit_kat":
    case "kat":
    	$kategori = "Category";
    	$edit = "Update"; break;

    case "edit_div":
    case "div":
    	$division = "Divisi";
    	$edit = "Update"; break;

    case "edit_user":
    case "user":
    	$user = "User";
        $edit = "Update"; break;

    case "profile":
     $profil = "Profile"; break;
     
    default:
      $home = "Main Page"; break;
  }
?>
<h1>
	<?php echo $user . $doc . $division . $kategori . $home . $profil; ?>
	<small><?php echo $tambah . $edit; ?></small>
</h1>